﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Radix.Model;
using Radix.Service;

namespace Radix.Controllers
{
    [Produces("application/json")]
    [Route("api/Employees")]
    public class EmployeesController : Controller
    {
        private ICrudable<Employee> _employeeService;

        public EmployeesController(ICrudable<Employee> employeeService)
        {
            _employeeService = employeeService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_employeeService.GetAll());
        }

       
        [HttpGet]
        [Route("{id}")]
        public IActionResult Get(int id)
        {
            Employee employee = _employeeService.Get(id).Result;
            if (employee == null)
            {
                return NotFound();
            }
            return Ok(employee);
        }

        [HttpPost]
        public IActionResult Post(Employee employee)
        {
            try
            {
                _employeeService.Create(employee);
                return Ok(employee);
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest.ToString());
            }
        }
    }
}