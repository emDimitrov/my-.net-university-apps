﻿using Microsoft.EntityFrameworkCore;
using Radix.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Radix.Service
{
    public class EmployeeContext : DbContext
    {
        public EmployeeContext(DbContextOptions<EmployeeContext> options)
            :base(options)
        {

        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Departments { get; set; }
    }
}
