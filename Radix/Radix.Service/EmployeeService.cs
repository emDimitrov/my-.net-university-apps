﻿using Microsoft.EntityFrameworkCore;
using Radix.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Radix.Service
{

    public class EmployeeService : CrudService<Employee>
    {
        public EmployeeService(EmployeeContext dbContext) : base(dbContext)
        {
        }
    }
}
