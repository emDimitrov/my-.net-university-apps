﻿using Microsoft.EntityFrameworkCore;
using Radix.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Radix.Service
{
    public class CrudService<TEntity> : ICrudable<TEntity>
        where TEntity : BaseEntity
    {
        private readonly EmployeeContext _dbContext;

        private readonly DbSet<TEntity> dbSet;

        public CrudService(EmployeeContext dbContext)
        {
            _dbContext = dbContext;
            dbSet = dbContext.Set<TEntity>();
        }

        #region GetAll
        public IQueryable<TEntity> GetAll()
        {
            return dbSet.AsNoTracking();
        }

        public IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> filter)
        {
            return dbSet.Where(filter).AsNoTracking();
        }
        #endregion

        #region Get
        public async Task<TEntity> Get(int id)
        {
            return await dbSet
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task<TEntity> Get(Expression<Func<TEntity, bool>> filter)
        {
            return await dbSet
                .AsNoTracking()
                .FirstOrDefaultAsync(filter);
        }
        #endregion

        #region Create
        public virtual Task OnBeforeCreate(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public async Task Create(TEntity entity)
        {
            await dbSet.AddAsync(entity);
            await _dbContext.SaveChangesAsync();
        }

        public virtual Task OnAfterCreate()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Update
        public virtual Task OnBeforeUpdate(TEntity entity)
        {
            throw new NotImplementedException();
        }
        public async Task Update(TEntity entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }

        public virtual Task OnAfterUpdate()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Delete
        public virtual Task OnBeforeDelete(TEntity entity)
        {
            throw new NotImplementedException();
        }
        public async Task Delete(int id)
        {
            var entity = await Get(id);
            _dbContext.Set<TEntity>().Remove(entity);
            await _dbContext.SaveChangesAsync();
        }

        public virtual Task OnAfterDelete()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
