﻿using Radix.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Radix.Service
{
    public interface ICrudable<TEntity>
        where TEntity : BaseEntity
    {
        IQueryable<TEntity> GetAll();

        IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> filter);

        Task<TEntity> Get(int id);

        Task<TEntity> Get(Expression<Func<TEntity, bool>> filter);

        Task OnBeforeCreate(TEntity entity);
        Task Create(TEntity entity);
        Task OnAfterCreate();

        Task OnBeforeUpdate(TEntity entity);
        Task Update(TEntity entity);
        Task OnAfterUpdate();

        Task OnBeforeDelete(TEntity entity);
        Task Delete(int id);
        Task OnAfterDelete();
    }
}
