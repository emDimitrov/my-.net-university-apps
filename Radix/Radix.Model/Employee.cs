﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Radix.Model
{
    public class Employee : BaseEntity
    {
        public string Name { get; set; }

        public int DepartmentId { get; set; }

        public Department Department { get; set; }
    }
}
