﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Radix.Model
{
    public class Department : BaseEntity
    {
        public string Address { get; set; }

        public List<Employee> Employees { get; set; }
    }
}
