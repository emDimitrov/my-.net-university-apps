﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using TestApp.App_Start;
using TestApp.Entities;
using TestApp.ViewModels;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Type hai = Type.GetType("Person", true);
            Object o = (Activator.CreateInstance(hai));
            Console.WriteLine(o.GetType().Name);
            Console.ReadKey();
        }
    }

    public class Person
    {
        public string Name { get; set; }
    }
}
