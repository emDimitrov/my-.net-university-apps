﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.App_Start
{
    class MapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
                cfg.AddProfiles(Assembly.GetExecutingAssembly()));
        }
    }
}
