﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingApp
{
    public class Zoo<T> where T : Animal, new()
    {
        public List<T> animals;

        public Zoo()
        {
            animals = new List<T>();
        }
        public void PrintAllAnimals()
        {
            foreach (var animal in animals)
            {
                foreach (var pi in animal.GetType().GetProperties())
                {
                    Console.WriteLine(pi.Name+ "    "+pi.GetValue(animal));
                }
            }
        }
    }
}
