﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingApp
{
    public class Animal : IIntroducable
    {
        private int id;
        private string name;

        public int EGN { get; set; }
        public string PARENTS { get; set; }

        public Animal(int id, string name)
        {
            this.id = id;
            this.name = name;
        }

        public Animal()
        {

        }

        public virtual void SayMyName()
        {
            Console.WriteLine("My name is ANIMAL.");
        }

        
    }
}
