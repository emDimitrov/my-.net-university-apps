﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingApp
{
    public class Gosho : Human
    {
        public int Age { get; set; }

        public override void SayBye()
        {
            Console.WriteLine("GOSHO");
        }
    }
}
