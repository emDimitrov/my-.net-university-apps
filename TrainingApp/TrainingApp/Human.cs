﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingApp
{
    public abstract class Human
    {
        public string Name { get; set; }
        public abstract void SayHi();

        public virtual void SayBye()
        {
            Console.WriteLine("HUMAN");
        }
    }
}
