﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingApp
{
    public class Monkey : Animal
    {
        public int ID { get; set; }
        public string NAME { get; set; }
        public int MONKEY { get; set; }

        private Monkey(int id, string name) : base(id, name)
        {
        }

        public Monkey() : base()
        {
            
        }

        public  void SayMyName()
        {
            Console.WriteLine("My name is a monkey. 222  ");
        }
    }
}
