﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NotesToDoApp.Models
{
    public class Note
    {
        public int ID { get; set; }

        [Required]
        [StringLength(60, MinimumLength = 3)]
        public string Title { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        [Required]
        public bool IsDone { get; set; }
    }

    public class NoteDBContext : DbContext
    {
        public DbSet<Note> Notes { get; set; }
    }
}