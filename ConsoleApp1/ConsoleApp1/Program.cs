﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Insert upper bound: ");
            int upperBound = int.Parse(Console.ReadLine());

            Console.Write("Insert lower bound: ");
            int lowerBound = int.Parse(Console.ReadLine());

            Console.Write("Insert number to check: ");
            string numberToValidate = Console.ReadLine();

            if(CheckNumberIfInRange(numberToValidate, upperBound, lowerBound))
            {
                Console.WriteLine(numberToValidate + " is in range [" + lowerBound + " - " + upperBound + "]");
            } else
            {
                Console.WriteLine(numberToValidate + " number is not in the specified range or not a valid number");
            }
            Console.ReadKey();
        }

        static bool CheckNumberIfInRange(string text, int upperBound, int lowerBound)
        {
            int validNumber;
            if (Int32.TryParse(text, out validNumber))
            {
                if(validNumber > lowerBound && validNumber < upperBound)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
