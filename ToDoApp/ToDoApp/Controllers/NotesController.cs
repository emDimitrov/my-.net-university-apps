﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ToDoApp.DataAccess;
using ToDoApp.Models;

namespace ToDoApp.Controllers
{
    public class NotesController : Controller
    {
       
        public ActionResult Index()
        {
            NoteRepository repo = new NoteRepository();
            List<Note> model = repo.GetAll();

            return View(model);
        }

        [HttpPost]
        public ActionResult Action(int id)
        {

            NoteRepository repo = new NoteRepository();
            Note note = repo.Get(id);
            note.IsDone = true;
            repo.Update(note);

            return View(note);
        }

        public JsonResult GetNotes()
        {
            NoteRepository repo = new NoteRepository();
            List<Note> Notes = repo.GetAll();
            return Json(Notes, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Details(int id)
        {
            NoteRepository repo = new NoteRepository();
            Note note = repo.Get(id);

            if (note == null)
            {
                return HttpNotFound();
            }
            return View(note);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Note model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            NoteRepository repository = new NoteRepository();
            repository.Insert(model);

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            NoteRepository repository = new NoteRepository();
            Note model = repository.Get(id);

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Note model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            NoteRepository repository = new NoteRepository();
            repository.Update(model);

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            NoteRepository repository = new NoteRepository();
            Note model = repository.Get(id);

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NoteRepository repository = new NoteRepository();
            Note note = repository.Get(id);

            if (ModelState.IsValid)
            {
                repository.Delete(note);

                return RedirectToAction("Index");
            }

            return View(note);
        }
    }
}