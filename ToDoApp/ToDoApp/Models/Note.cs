﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ToDoApp.Models
{
    public class Note : BaseModel
    {
        [Required]
        [StringLength(60, MinimumLength = 5)]
        public string Title { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        [Display(Name = "Is Done")]
        public bool IsDone { get; set; }
    }
}