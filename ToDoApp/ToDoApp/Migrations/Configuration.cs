namespace ToDoApp.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using ToDoApp.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<ToDoApp.DataAccess.ToDoAppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ToDoApp.DataAccess.ToDoAppContext context)
        {
        }
    }
}
