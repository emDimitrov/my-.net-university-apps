// MatrixApp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>
#include <array>
#include <vector>
#include <iostream>
#include <limits>
#include <stdio.h>
using namespace std;

int rowA = 0;
int colA = 0;
void ReadDataFile(int matrix[3][3]);
void DisplayData(int inputArray[3][3]);
int FindColumnLargest(int inputArray[3][3]);
int FindRowLowest(int inputArray[3][3]);
vector<int> ExtractMiddleNumbers(int inputArray[3][3], int maxNumber, int minNumber);
void CreateExtractedMatrix(vector<int>& numbers);

int main()
{
	cout << "--------Matrix Application--------\n" << endl;
	cout << "     ***Matrix***\n" << endl;

	int matrix[3][3] = { {0} };
	ReadDataFile(matrix);
	DisplayData(matrix);

	int maxNumber = FindColumnLargest(matrix);
	int minNumber = FindRowLowest(matrix);
	vector<int> extractedNumbers= ExtractMiddleNumbers(matrix, maxNumber, minNumber);
	CreateExtractedMatrix(extractedNumbers);

	return 0;
}

void ReadDataFile(int matrix[3][3])
{
	string line;
	int element;
	ifstream fileIn("matrix.txt");

	//Error check
	if (fileIn.fail())
	{
		cerr << "File not found";
		exit(1);
	}

	while (fileIn.good())
	{
		while (getline(fileIn, line))
		{
			istringstream streamA(line);
			colA = 0;
			while (streamA >> element)
			{
				matrix[rowA][colA] = element;
				colA++;
			}

			rowA++;
		}
	}
}

void DisplayData(int inputArray[3][3])
{
	for (int row = 0; row < rowA; row++)
	{
		for (int col = 0; col < colA; col++)
		{
			cout << left << setw(6) << inputArray[row][col] << " ";
		}

		cout << endl;
	}
}

int FindColumnLargest(int inputArray[3][3])
{
	int maxNum = 0;
	int rowIndex = 0;
	for (int row = 0; row < 3; row++)
	{
		if (inputArray[row][0] > maxNum)
		{
			maxNum = inputArray[row][0];
			rowIndex = row;
		}
	}

	cout << "The largest number in first column is: " << maxNum << " - [" << rowIndex + 1 << "," << "1" << "]" << endl;
	cout << "--------------------------------------" << endl;

	return maxNum;
}

int FindRowLowest(int inputArray[3][3])
{
	int minNum = std::numeric_limits<int>::max();
	int colIndex = 0;
	for (int col = 0; col < 3; col++)
	{
		if (inputArray[2][col] < minNum)
		{
			minNum = inputArray[2][col];
			colIndex = col;
		}
	}

	cout << "The lowest number in last row is: " << minNum << " - [3," << colIndex + 1 << "]"<< endl;
	cout << "--------------------------------------\n" << endl;

	return minNum;
}

vector<int> ExtractMiddleNumbers(int inputArray[3][3], int maxNumber, int minNumber)
{
	vector<int> numbersContainer;

	for (int row = 0; row < 3; row++)
	{
		for (int col = 0; col < 3; col++)
		{
			int currentNumber = inputArray[row][col];
			if (currentNumber > minNumber && currentNumber < maxNumber)
			{
				numbersContainer.push_back(currentNumber);
				numbersContainer.push_back(row + 1);
				numbersContainer.push_back(col + 1);
			}
		}
	}

	return numbersContainer;
}

void CreateExtractedMatrix(vector<int>& numbers)
{
	int extractedMatrix[10][3] = { { 0 } };

	int counter = 0;
	for (int row = 0; row < 7; row++)
	{
		for (int col = 0; col < 3; col++)
		{
			if (counter < numbers.size())
			{
				extractedMatrix[row][col] = numbers[counter];
				counter++;
			}
			else
			{
				break;
			}
		}
	}

	//Display new matrix
	cout << "     ***New Matrix***\n" << endl;
	cout << "[Value][Row][Col]" << endl;
	for (int row = 0; row < 7; row++)
	{
		for (int col = 0; col < 3; col++)
		{
			cout << left << setw(6) << extractedMatrix[row][col] << " ";
		}
		cout << endl;
	}
}