﻿using Relationships.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Relationships.Tools
{
    public abstract class Sorter
    {
        private static int Partition(Friendship[] arr, int low, int high)
        {
            int pivot = arr[low].FirstStudent.FacultyNumber;
            int i = low - 1;
            int j = high + 1;

            while (true)
            {
                // Find leftmost element greater than
                // or equal to pivot
                do
                {
                    i++;
                } while (arr[i].FirstStudent.FacultyNumber < pivot);

                // Find rightmost element smaller than
                // or equal to pivot
                do
                {
                    j--;
                } while (arr[j].FirstStudent.FacultyNumber > pivot);

                // If two pointers met
                if (i >= j)
                {
                    return j;
                }

                Swap(ref arr[i],ref arr[j]);
            }
        }

        public static void QuickSort(Friendship[] arr, int low, int high)
        {
            if (low < high)
            {
                int pi = Partition(arr, low, high);

                // Separately sort elements before
                // and after partition
                QuickSort(arr, low, pi);
                QuickSort(arr, pi + 1, high);
            }
        }

        static private void Swap(ref Friendship a, ref Friendship b)
        {
            Friendship tmp = a;
            a = b;
            b = tmp;
        }
    }
}
