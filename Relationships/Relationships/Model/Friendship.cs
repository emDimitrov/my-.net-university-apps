﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Relationships.Model
{
    public class Friendship
    {
        public Student FirstStudent { get; private set; }
        public Student SecondStudent { get; private set; }

        public Friendship(Student firstStudent, Student secondStudent)
        {
            this.FirstStudent = firstStudent;
            this.SecondStudent = secondStudent;
        }
    }
}
