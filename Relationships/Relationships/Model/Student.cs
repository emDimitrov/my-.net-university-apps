﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Relationships.Model
{
    public class Student : IComparable
    {
        public int FacultyNumber { get; private set; }
        public string Name { get; private set; }
        public Gender Gender { get; private set; }

        public Student(int facultyNumber, string name, Gender gender)
        {
            this.FacultyNumber = facultyNumber;
            this.Name = name;
            this.Gender = gender;
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            Student student = obj as Student;
            if (student != null)
                return this.FacultyNumber.CompareTo(student.FacultyNumber);
            else
                throw new ArgumentException("Object is not a Student");
        }
    }

    public enum Gender { Male, Female}
}
