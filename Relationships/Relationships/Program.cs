﻿using Relationships.Data;
using Relationships.Model;
using Relationships.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Relationships
{
    class Program
    {
        static Student[] Students;
        static Friendship[] Friendships;

        static void Main(string[] args)
        {
            DbManager.LoadTestData(ref Students, ref Friendships);

            Sorter.QuickSort(Friendships, 0, Friendships.Length-1);
            int studentId = ValidateId();

            FriendshipManager friendshipManager = new FriendshipManager();
            Friendship[] studentFriendships = friendshipManager.GetStudentFriendships(Friendships, studentId);

            List<Student> friendsOfMine = new List<Student>();
            List<Student> friendsPals = new List<Student>();
            
            foreach (var item in studentFriendships)
            {
                Console.WriteLine("-------------------------");
                Console.WriteLine("Friend name: " + item.SecondStudent.Name+"\n");
                friendsOfMine.Add(item.SecondStudent);

                Console.Write("Her/His friends are: ");
                foreach (var friendsRelatives in friendshipManager
                            .GetStudentFriendships(Friendships,item.SecondStudent.FacultyNumber))
                {
                    Console.Write(friendsRelatives.SecondStudent.Name+"| ");
                    friendsPals.Add(friendsRelatives.SecondStudent);
                }
                Console.WriteLine();
            }

            List<Student> unknownPeople = friendshipManager.GetUnknownPeople(friendsOfMine, friendsPals);
            if (unknownPeople.Count != 0)
            {
                Console.WriteLine("\n-----Unknown people-----");
                foreach (var person in unknownPeople)
                {
                    Console.WriteLine(person.Name);
                }
            }
            
            Console.ReadKey();
        }

        static int ValidateId()
        {
            int studentId;
            while (true)
            {
                Console.Write("Enter Id of the student: ");
                string input = Console.ReadLine();

                if (int.TryParse(input, out studentId))
                {
                    studentId = int.Parse(input);
                    break;
                }
                else
                {
                    Console.WriteLine("Only numbers are allowed\n");
                    continue;
                }
            }

            return studentId;
        }
    }
}
