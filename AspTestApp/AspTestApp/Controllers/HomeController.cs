﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspTestApp.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            IEnumerable<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem { Text = "Action", Value = "0" },

                new SelectListItem { Text = "Drama", Value = "1" }
            };
            ViewBag.list = items;
            return View();
        }
    }
}