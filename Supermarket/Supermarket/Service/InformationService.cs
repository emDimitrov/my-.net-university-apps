﻿using Supermarket.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supermarket.Service
{
    public class InformationService
    {
        private OrdersTrackingService _ordersTrackingService;

        public InformationService(OrdersTrackingService ordersTrackingService)
        {
            this._ordersTrackingService = ordersTrackingService;
        }

        public List<Product> GetManufacturerSales(string manufacturer)
        {
            List<Product> manufacturerSales = new List<Product>();
            foreach (var product in ExtractProducts())
            {
                if (product.Manufacturer.ToLower() == manufacturer)
                {
                    manufacturerSales.Add(product);
                }
            }

            return manufacturerSales;
        }

        public List<Order> GetSalesByDay(DateTime salesDate)
        {
            List<Order> salesByDay = new List<Order>();
            foreach (var order in _ordersTrackingService.Orders)
            {
                if (order.TimeOfOrder == salesDate)
                {
                    salesByDay.Add(order);
                }
            }

            return salesByDay;
        }

        public void GetSoldPromotions()
        {
            List<string> promotionNames = new List<string>();
            List<Product> promotionalProducts = ExtractProducts().Distinct().ToList();
            List<Order> orders = _ordersTrackingService.Orders;

            foreach (var product in promotionalProducts)
            {
                promotionNames.Add(product.Name);
            }

            foreach (var order in orders)
            {

            }
        }

        public List<Product> ExtractProducts()
        {
            List<Product> products = new List<Product>();
            foreach (var order in _ordersTrackingService.Orders)
            {
                foreach (var product in order.Products)
                {
                    products.Add(product);
                }
            }

            return products;
        }

        public List<Product> ExtractProducts(List<Order> orders)
        {
            List<Product> products = new List<Product>();
            foreach (var order in orders)
            {
                foreach (var product in order.Products)
                {
                    products.Add(product);
                }
            }

            return products;
        }
    }
}
