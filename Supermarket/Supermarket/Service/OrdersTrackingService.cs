﻿using Supermarket.Model;
using Supermarket.Strategy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supermarket.Service
{
    public class OrdersTrackingService
    {
        public List<Order> Orders { get; set; }
        private PaymentStrategy _paymentStrategy;

        public OrdersTrackingService(PaymentStrategy paymentStrategy)
        {
            this._paymentStrategy = paymentStrategy;
            Orders = new List<Order>();  
        }

        public void SaveOrder(Order order)
        {
            SetStrategy(order);
            _paymentStrategy.Calculate(order);
            Orders.Add(order);
        }

        public void SetStrategy(Order order)
        {
            for (int i = 0; i < order.Products.Count; i++)
            {
                if (order.Products[i].IsPromotional)
                {
                    _paymentStrategy = new DiscountStrategy();
                    break;
                }
            }
        }
    }
}
