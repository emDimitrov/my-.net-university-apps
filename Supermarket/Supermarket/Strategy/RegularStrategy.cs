﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Supermarket.Model;
using Supermarket.Service;

namespace Supermarket.Strategy
{
    class RegularStrategy : PaymentStrategy
    {
        public override decimal Calculate(Order order)
        {
            foreach (var product in order.Products)
            {
                order.Price += product.Price;
            }

            return order.Price;
        }
    }
}
