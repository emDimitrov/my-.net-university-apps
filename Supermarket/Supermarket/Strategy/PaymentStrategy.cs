﻿using Supermarket.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supermarket.Strategy
{
    public abstract class PaymentStrategy
    {
        public abstract decimal Calculate(Order order);
    }
}
