﻿using Supermarket.Model;
using Supermarket.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supermarket.Strategy
{
    public class DiscountStrategy : PaymentStrategy
    {
        public override decimal Calculate(Order order)
        {
            List<Product> promotionalProducts = new List<Product>();
            
            foreach (var product in order.Products)
            {
                if (!product.IsPromotional)
                {
                    order.Price += product.Price;
                }
                else
                {
                    promotionalProducts.Add(product);
                }
            }

            if (promotionalProducts.Count > 1)
            {
                order.Price += MakeTwoForOneDiscount(promotionalProducts, order);
            }

            return order.Price;
        }

        private decimal MakeTwoForOneDiscount(List<Product> promotionalProducts, Order order)
        {
            decimal totalPrice = 0;
            int promotionsPerOrder = 0;
            List<int> productIds = GetPromoIds(promotionalProducts);
            for (int i = 0; i < productIds.Distinct().Count(); i++)
            {
                List<Product> sameProducts = promotionalProducts.FindAll(p => p.Id == productIds[i]);
                if (sameProducts.Count % 2 != 0)
                {
                    sameProducts.RemoveAt(sameProducts.Count - 1);
                    for (int x = 0; x < sameProducts.Count; x++)
                    {
                        totalPrice += sameProducts[i].Price / 2.0m;
                    }

                    promotionsPerOrder = (sameProducts.Count - 1 / 2);
                    totalPrice += sameProducts.First().Price;
                }
                else
                {
                    promotionsPerOrder = (sameProducts.Count) / 2;
                    for (int x = 0; x < sameProducts.Count; x++)
                    {
                        totalPrice += sameProducts[i].Price / 2.0m;
                    }
                }             
            }

            order.SoldPromotions = promotionsPerOrder;
            return totalPrice;
        }

        private List<int> GetPromoIds(List<Product> promotionalProducts)
        {
            List<int> productIds = new List<int>();
            foreach (var product in promotionalProducts)
            {
                productIds.Add(product.Id);
            }

            return productIds;
        }
    }
}
