﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supermarket.Model
{
    public class Beer : Product
    {
        public Beer(int id, string name, double netQuantity, string manufacturer, decimal price, bool isPromotional)
            : base(id, name, netQuantity, manufacturer, price, isPromotional)
        {

        }
    }
}
