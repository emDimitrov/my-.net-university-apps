﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supermarket.Model
{
    abstract public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double NetQuantity { get; set; }
        public string Manufacturer { get; set; }
        public decimal Price { get; set; }
        public bool IsPromotional { get; set; }

        public Product(int id, string name, double netQuantity, string manufacturer, decimal price, bool IsPromotional)
        {
            this.Id = id;
            this.Name = name;
            this.NetQuantity = netQuantity;
            this.Manufacturer = manufacturer;
            this.Price = price;
            this.IsPromotional = IsPromotional;
        }
    }
}
