﻿using Supermarket.Strategy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supermarket.Model
{
    public class Order
    {
        public int Id { get; set; }
        public List<Product> Products { get; private set; }
        public int SoldPromotions { get; set; }
        public decimal Price { get; set; }
        public DateTime TimeOfOrder { get; set; }

        public Order(int id, DateTime timeOfOrder)
        {
            this.Id = id;
            this.Products = new List<Product>();
            this.TimeOfOrder = timeOfOrder;
        }

        public void AddProduct(Product product, int quantity)
        {
            if (quantity < 2)
            {
                product.IsPromotional = false;
            }
            for (int i = 0; i < quantity; i++)
            {
                Products.Add(product);
            }
        }

        public void RemoveProduct(int productId)
        {
            Product productToRemove = Products.Find(x => x.Id == productId);
            Products.Remove(productToRemove);
        }
    }
}
