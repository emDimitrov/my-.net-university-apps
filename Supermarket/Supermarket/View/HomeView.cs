﻿using Supermarket.Model;
using Supermarket.Service;
using Supermarket.Strategy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supermarket.View
{
    public class HomeView
    {
        private OrdersTrackingService _ordersTrackingService;
        private InformationService _informationService;

        public HomeView()
        {
            Beer burgasko = new Beer(0, "Burgasko", 0.5, "Burgas", 2.10m, true);
            Beer zagorka = new Beer(1, "Zagorka", 0.5, "Stara Zagora", 2.50m, false);
            Beer kamenitza = new Beer(2, "Kamenitza", 0.5, "Silistra", 1.80m, true);

            Order order = new Order(1, new DateTime(2018, 3, 1));
            order.AddProduct(burgasko, 3);
            order.AddProduct(zagorka, 2);

            Order order2 = new Order(2, new DateTime(2018, 4, 2));
            order2.AddProduct(kamenitza, 2);
            

            PaymentStrategy paymentStrategy = new RegularStrategy();

            _ordersTrackingService = new OrdersTrackingService(paymentStrategy);
            _informationService = new InformationService(_ordersTrackingService);
           
            _ordersTrackingService.SaveOrder(order);
            _ordersTrackingService.SaveOrder(order2);

            View();

        }

        public void View()
        {
            while (true)
            {
                Console.WriteLine("##########################################");
                Console.WriteLine("#          Information Service           #");
                Console.WriteLine("#                                        #");
                Console.WriteLine("#          [M]anufacturer Sales          #");
                Console.WriteLine("#          [P]romotions Sold             #");
                Console.WriteLine("#          [S]ales by Date               #");
                Console.WriteLine("#          E[x]it                        #");
                Console.WriteLine("##########################################");

                ConsoleKeyInfo input = Console.ReadKey();
                switch (input.Key)
                {
                    case ConsoleKey.M:
                        {
                            Console.Clear();
                            ShowManufacturerSales();
                            continue;
                        }
                    case ConsoleKey.P:
                        {
                            Console.Clear();
                            ShowSoldPromotions();
                            continue;
                        }
                    case ConsoleKey.S:
                        {
                            Console.Clear();
                            ShowSalesByDay();
                            continue;
                        }
                    case ConsoleKey.X:
                        {
                            Environment.Exit(0);
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid choice.");
                            Console.ReadKey(true);
                            break;
                        }
                }
            }            
        }

        public void ShowManufacturerSales()
        {
            Console.WriteLine("************** Sales Information **************");
            foreach (var product in _informationService.ExtractProducts().Distinct())
            {
                List<Product> manufacturerProducts = _informationService.GetManufacturerSales(product.Manufacturer.ToLower());
                Console.WriteLine("                                           ");
                Console.WriteLine("Manufacturer: {0}", product.Manufacturer);
                Console.WriteLine("{0} {1}", manufacturerProducts.FirstOrDefault().GetType().Name, manufacturerProducts.FirstOrDefault().Name);
                Console.WriteLine("Sales: {0}", manufacturerProducts.Count);
                Console.WriteLine("                                           ");
                Console.WriteLine("----------------------------------------------");
            }
            Console.WriteLine("                                           ");
            Console.WriteLine("************* " + DateTime.Now+ " *************");
        }

        public void ShowSalesByDay()
        {
            Console.WriteLine("************** Sales Information **************");    
            Console.WriteLine("                                               ");
            List<DateTime> orderDates = new List<DateTime>();
            foreach (var order in _ordersTrackingService.Orders)
            {
                orderDates.Add(order.TimeOfOrder);
            }

            foreach (var date in orderDates.Distinct())
            {
                List<Order> orders = _informationService.GetSalesByDay(date);
                decimal income = 0;
                foreach (var order in orders)
                {
                    income += order.Price;
                }

                Console.WriteLine("---- Sales made on: {0} ----", date);
                List<Product> products = _informationService.ExtractProducts(orders);
                foreach (var product in products.Distinct())
                {
                    List<Product> manufacturerProducts = _informationService.GetManufacturerSales(product.Manufacturer.ToLower());
                    Console.WriteLine("                                           ");
                    Console.WriteLine("Manufacturer: {0}", product.Manufacturer);
                    Console.WriteLine("{0} {1}", manufacturerProducts.FirstOrDefault().GetType().Name, manufacturerProducts.FirstOrDefault().Name);
                    Console.WriteLine("Sales: {0}", manufacturerProducts.Count);
                    Console.WriteLine("                                           ");
                    Console.WriteLine("----------------------------------------------");
                }
                Console.WriteLine("Total Income: {0}",income);
                Console.WriteLine();
            }

            Console.WriteLine("                                           ");
            Console.WriteLine("************* " + DateTime.Now + " *************");
        }

        public void ShowSoldPromotions()
        {
            Console.WriteLine("************** Sales Information **************");
            List<Order> orders = _ordersTrackingService.Orders;
            foreach (var order in orders)
            {
                Console.WriteLine("Order with ID: {0} ",order.Id);
                Console.WriteLine("Promotions sold: {0}",order.SoldPromotions);
                Console.WriteLine("----------------------------------------------");
            }
            Console.WriteLine("                                           ");
            Console.WriteLine("************* " + DateTime.Now + " *************");
        }
    }
}
